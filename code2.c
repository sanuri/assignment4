#include<stdio.h>
#define pi 3.14

int main() {
	float r, h, v;
	printf("Enter radius of the cone: ");
	scanf("%f", &r);
	printf("Enter height of the cone: ");
	scanf("%f", &h);
	
	v=(pi * r * r * h)/3;
	printf("\nThe volume of the cone = %f", v);
	return 0;
	
}
