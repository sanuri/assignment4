#include<stdio.h>
int main()
{
	int A = 10;
	int B = 15;
	int Z = 0;
	
	Z = A&B;
	printf("Bitwise AND operator- A&B = %d\n",Z );
	
	Z = A^B;
	printf("Bitwise XOR operator- A^B = %d\n",Z );
	
	Z = ~A;
	printf("Bitwise One's Compliment operator- ~A = %d\n", Z );
	
	Z = A<<3;
	printf("Bitwise Left Shift operator- A<<3 = %d\n",Z);
	
	
	Z = B>>3;
	printf("Bitwise Right Shift operator- B>>3 = %d\n",Z);
	
	return 0;
}
